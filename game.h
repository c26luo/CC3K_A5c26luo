#ifndef GAME_H
#define GAME_H
#include "display.h"
#include "cell.h"
#include <map>
#include <fstream>
using namespace std;

class Game {
	map< pair<int, int>, Cell *> Room0;
	map< pair<int, int>, Cell *> Room1;
	map< pair<int, int>, Cell *> Room2;
	map< pair<int, int>, Cell *> Room3;
	map< pair<int, int>, Cell *> Room4;
	map< pair<int, int>, Cell *> Passage;
	//map< pair<int, int>, GameObject *> Monster_Location;

	int width, height;
	int turns;
	Display * theDisplay;
	//Character * player;
	
// Private Functions:
	//void Tick()
		// turns++
		// is going to move every monster monster move
		// GameObject * say this is called name Mob
		// im going to call Mob->move() Cedric implement
	void initCells(int, int, map< pair<int, int>, Cell *>);

public:
	Game(ifstream& infile);
	~Game();
	void print();
	Display * getDisplay();
	void init_game();
	void init_neighbours();
	//void resetFloor();

	// The following Methods needs GameObject Class and its Subclass to implement:
	//void command_u(string direction);
		//	 will call Player->Action and will call Tick()
	//void command_a(string direction);
		//	 will call Player->Attack and will call Tick()
	//void move(string direction);
		//	 will call Player->Move and will call Tick()
	// int getTurns();
	// int getHP();
	// int getStats();
	// bool isDead();
};

#endif
