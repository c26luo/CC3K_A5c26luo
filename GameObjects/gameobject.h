#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

// forward declaration because mutual reference
class Cell;

// add public/private/protected members as necessary
class GameObject {
    Cell * _location;
    // int x;
    // int y; // not sure about those
    public:
        GameObject(int x,int y, Cell *location);

        int x();  // getters
        int y();

        void x(int n);  // setters
        void y(int n);

        // Called after every turn
         virtual void tick() = 0;

        
};

#endif
