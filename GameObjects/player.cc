#include "player.h"
#include <string>
using namespace std;

void Player::setMAXHP(int hp) {
        MAXHP = hp;
}
/*
void Player::tick(){
        _location->notifyGame(); // Notify game about increase in turn;
        routine_iterate_through_all_mobs_telling_them_that_Player_did_action // dont know how this will work

}
*/

int intDirection(string direction) {
        int intDirection;
        if (direction == "nw") {
                intDirection = 0;
        }
        if (direction == "no") {
                intDirection = 1;
        }
        if (direction == "ne") {
                intDirection = 2;
        }
        if (direction == "we") {
                intDirection = 3;
        }
        if (direction == "ea") {
                intDirection = 4;
        }
        if (direction == "sw") {
                intDirection = 5;
        }
        if (direction == "so") {
                intDirection = 6;
        }
        if (direction == "se") {
                intDirection = 7;
        }
        return intDirection;
}

void Player::moveDirection(string direction) {
        int direction = intDirection(direction); // translate directions to integer
        // if (!isSurrounded());
        vector <Cell *> emptyCells = neighbours.findFreeCell(); // Default for Player????
        move(emptyCells.at(direction));
        if ( (_health + 5) >= MAXHP ) { setHealth(MAXHP)}
        else setHealth(_health + 5);
        tick();
}

void attackDirection(string direction) {
        int direction = intDirection(direction);

        if (doesCellcontainsMonster()) {  // needs to check if cell contains monster
                Character *mob = findMonster(); // both routine i dont know;
                attack(mob);

        } else {
                // output an error to stderr and read in another valid command
        }
        tick();
}
