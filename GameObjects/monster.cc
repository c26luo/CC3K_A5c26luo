#include "monster.h"
#include "PRNG.h"

void Monster::tick() {
	checkPlayer();
}

void Monster::moveRandom() {
	// if (!isSurrounded()) {}
	vector <Cell *>  emptyCells = _location->findFreeCell()
	int numCellsEmpty = emptyCells.size();
	if (numCellsEmpty == 0) {} // don't move
	else {
		PRNG p(seed); // FIND HOW TO GET SEED, use a global variable for seed?
		move(emptyCells.at(p(numCellsEmpty) - 1));
	}

}

// (bool/void) Monster::checkPlayer() // should it be able to call move() and attack() depending on if it find player in its neighbours?

void Monster::attack() {

	// Character *player = findPlayer();
	attack(player);

}