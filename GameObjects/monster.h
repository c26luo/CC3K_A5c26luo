#ifndef MONSTER_H
#define MONSTER_H
#include "character.h"

class Monster : public Character {
	public:
		void moveRandom();
		// (bool/void) checkPlayer(); // returns whether the player is in the vector of neighbour; or returns the player pointer if found, NULL if not
		void attack();
		void tick();
};

#endif