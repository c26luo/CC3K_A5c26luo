#ifndef CHARACTER_H
#define CHARACTER_H
#include "gameobject.h"
#include <cmath>


class Character : public GameObject {
    int _health;
    int _attack;
    int _defence;
    

    public:
        // Character(...);


        int health(); // getters
        int attack();
        int defence();

        void setHealth(int hp); //setters
        void setAttack(int att);
        void setDefence(int def);

        void moveCharacter(Cell *cell);
        int damage(int attacker, int defender); // formula to calculate damage dealt
        void attack(Character *target);

        virtual void tick();    // override from GameObject


        
};

#endif
