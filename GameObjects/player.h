#ifndef PLAYER_H
#define PLAYER_H
#include "character.h"
#include <string>

class Player : public Character {
	int MAXHP;
    public: 
    	void setMAXHP(int hp);
    	void moveDirection(std::string direction);
    	void attackDirection(std::string direction);
    	int intDirection(std::string direction); // translate string to int
    	void tick();
};

#endif
