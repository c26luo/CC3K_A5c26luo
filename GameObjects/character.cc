#include "character.h"
#include <cmath>

void Character::setHealth(int hp) {
	_health = hp;
}

void Character::setAttack(int att) {
	_attack = att;
}

void Character::setDefence(int def) {
	_defence = def;
}

int Character::health() { return _health; }

int Character::attack() { return _attack; }

int Character::defence(){ return _defence; }


void Character::moveCharacter(Cell *newCell) {
    Cell *temp = _location;
    oldDisplay = temp->display();

    newCell->setDisplay(oldDisplay); // set cell moved to content and display;
    newCell->setContent(this);

    _location->removeObject(); // set cell content to NULL
    _location->setContent('.'); // set cell display to '.'
    _location = newCell // update Character location;
}

int Character::damage(int attker, int defender) {
    int formula = ( (attker * (100 - defender)) / 100)
    return ceil(formula);
}

void Character::attack(Character *target) {
    
    int damage = damage(_attack, target->defence());
    target->setHealth(target->health() - damage);
}