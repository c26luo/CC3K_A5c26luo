#include "samurai.h"
#include "merchant.h"
#include "goblin.h"
#include "gridbug.h"
#include "dragon.h"
#include "orc.h"

Samurai::Samurai() {
	setHealth(80);
	setAttack(50);
	setDefence(15);
	setGold(0);
	setDefaultValues();
}

bool Samurai::attack(std::string dir) {
	std::vector<Cell *> neighbour = getLocation()->getNeighbours();
	int index = 246;
	int x = getLocation()->x();
	int y = getLocation()->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}
	if (index == 246) {
		std::cout << "There is nothing there to attack!" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "Goblin") {
		Goblin::turnhostile();
	}
	else if (neighbour.at(index)->get_contents_name() == "Orc") {
		Orc::turnhostile();
	}
	else if (neighbour.at(index)->get_contents_name() == "Dragon") {
		Dragon::turnhostile();
	}
	else if (neighbour.at(index)->get_contents_name() == "Grid Bug") {
		GridBug::turnhostile();
	}
	else if (neighbour.at(index)->get_contents_name() == "Merchant") {
		Merchant::turnhostile();
	}
	else {
		std::cout << "There is nothing there to attack!" << std::endl;
		return false;
	}
	affect(neighbour.at(index)->getContents());
	int dmg = damage(getAttack(), neighbour.at(index)->getContents()->getDefence());
	std::cout << "You attack the " << neighbour.at(index)->getContents()->getName();
	std::cout << " with your Memory Corruption Katana for " << dmg << " damage!" << std::endl;
	return true;
}