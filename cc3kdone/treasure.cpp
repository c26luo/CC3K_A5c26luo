#include "treasure.h"

void Treasure::setName(std::string s) {
	name = s;
}

void Treasure::setAmount(int amt) {
	amount = amt;
}

std::string Treasure::getName() {
	return name;
}

void Treasure::setDisplay(char c) {
	_display = c;
}

void Treasure::setLocation(Cell * spawn_location) {
	location = spawn_location;
	location->setContent(this);
	location->setDisplay(_display);
}