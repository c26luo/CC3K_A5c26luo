#include "gold.h"

Gold::Gold(Cell * spawn_location) {
	setName("gold pile");
	setAmount(10);
	setDisplay('$');
	setLocation(spawn_location);
}
