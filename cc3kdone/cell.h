#ifndef CELL_H
#define CELL_H
#include <vector>
#include "game_object.h"
#include "display.h"

class GameObject;

class Cell {
	static Display* board;

	//Changes constantly:
	char _defaultdisplay;
	char _display;
	GameObject * _contents;

	int _x, _y;
	std::vector<Cell *> neighbours;
	bool monster_travelable;

public:
	Cell(int x, int y, char display, bool travel);
	//Getters:
	int x(); // returns x
	int y(); // returns y
	char display(); // returns display char
	bool getMonTravel();
	std::string get_contents_name();
	GameObject * getContents();

	// Setter:
	void setDisplay(char);
	void setContent(GameObject *);
	void removeObject(); // set GameObject to NULL
	void addDisplay(Display * display);

	// Related to Neighbours algorithm:
	void addNeighbour(Cell *); // adds a neighbour
	bool isEmpty();
	std::vector<Cell *> findFreeCell(); // Returns a vector of cell pointers that point to unoccupied cells.
	std::vector<Cell *> & getNeighbours();
};

#endif