#ifndef GAME_H
#define GAME_H
#include "prng.h"
#include "display.h"
#include "cell.h"
#include "game_object.h"
#include "player.h"
#include "knight.h"
#include "samurai.h"
#include "wizard.h"
#include "potion.h"
#include "monster.h"
#include "orc.h"
#include "goblin.h"
#include "gridbug.h"
#include "merchant.h"
#include "dragon.h"
#include "treasure.h"
#include "gold.h"
#include "dragonhoard.h"
#include "stairs.h"
#include <map>
#include <fstream>
#include <algorithm>
using namespace std;

extern PRNG prng;

class Game {
	// Object Maps
	map< pair<int, int>, Cell *> Room0;
	map< pair<int, int>, Cell *> Room1;
	map< pair<int, int>, Cell *> Room2;
	map< pair<int, int>, Cell *> Room3;
	map< pair<int, int>, Cell *> Room4;
	map< pair<int, int>, Cell *> Passage;
	map< pair<int, int>, Cell *> AllCells;
	vector<Monster *> Monster_List;

	//Data 
	int width, height;
	int turns;
	Display * theDisplay;
	Character * player;
	bool stopdeath;
	int floor;

// Private Functions:
	void initRooms(int, int, map< pair<int, int>, Cell *> &);
	void initPassage(int, int, map< pair<int, int>, Cell *> &);
	void initNeighbours();
	bool command_a(string dir);
	bool command_u(string direction);
	bool move(string direction);
	bool move(string direction, ifstream & map);
	void tick();
	int getTurns();
	int getHP();

	void spawn_map();
	void spawn_map(ifstream & map);
	void spawnplayer(int chamber);
	void spawnstairs(int);
	void spawnpotion(int);
	void spawngold(int);
	void spawnmonster(int);

	void resetfloor();
	void resetfloor(ifstream & map);
	void wipegame();

	static bool monster_comp_fn(Monster *, Monster *);
	static bool cell_comp_fn(Cell *, Cell *);
	void sort_monsterlist();
	void sort_cell_neighbours();
public:
	Game(ifstream& infile);
	~Game();
	void print();
	void play();
	void play(ifstream & map);
	//Testing purposes only
	void findFreeCells(int x, int y);
	Display * getDisplay();
};

#endif