#ifndef WIZARD_H
#define WIZARD_H
#include "player.h"

class Wizard : public Character {
	bool lazerbeam_attack(Cell * target, int x_dir, int y_dir);
public:
	Wizard();
	bool attack(std::string dir);
};


#endif