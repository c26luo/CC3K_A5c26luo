#include "knight.h"
#include "merchant.h"

Knight::Knight() {
	setHealth(100);
	setAttack(50);
	setDefence(50);
	setGold(0);
	setDefaultValues();
}

bool Knight::attack(std::string dir) {
	std::vector<Cell *> neighbour = getLocation()->getNeighbours();
	int index = 246;
	int x = getLocation()->x();
	int y = getLocation()->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}
	if (index == 246) {
		std::cout << "There is nothing there to attack!" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "nothing") {
		std::cout << "There is nothing there to attack!" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "Merchant") {
		Merchant::turnhostile();
	}
	affect(neighbour.at(index)->getContents());
	int dmg = damage(getAttack(), neighbour.at(index)->getContents()->getDefence());
	std::cout << "You attack the " << neighbour.at(index)->getContents()->getName();
	std::cout << " with your Sword of Segfault for " << dmg << " damage!" << std::endl;
	return true;
}