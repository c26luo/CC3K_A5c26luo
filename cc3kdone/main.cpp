#include "game.h"
#include "prng.h"
#include <sstream>

PRNG prng(217);

int main(int argc, char *argv[]) {
	if (argc == 1) {
		ifstream inMap("default_map.txt");
		Game game(inMap);
		game.play();
	}
	else if (argc == 2) { // Only one argument is given
		string s = string(argv[1]);
		std::istringstream ss(s);
		int n;
		if (ss >> n) {
			//agrv[1] is a number
			prng.seed(n);
			ifstream inMap("default_map.txt");
			Game game(inMap);
			game.play();
		}
		else {
			ifstream default_map("default_map.txt");
			ifstream readMap(argv[1]);
			Game game(default_map);
			game.play(readMap);
		}
	}
	else if (argc == 3) {

	}
	else {
		cout << "Invalid number of command arguments." << endl;
	}
	return 0;
}