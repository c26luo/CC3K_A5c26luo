#ifndef PLAYER_H
#define PLAYER_H
#include "game_object.h"
#include <cmath>

class Character : public GameObject {
	std::string name;
	std::string status;
	int maxhp;
	int _hp;
	int original_att;
	int _att;
	int original_def;
	int _def;
	int gold;
	Cell * location;
	bool is_descending;

public:
	// Get
	int getHealth();
	int getMaxHP();
	int getAttack();
	int getDefence();
	int getGold();
	std::string getStatus();
	std::string getName();
	Cell* getLocation();
	bool getDescending();

	//Set
	void setHealth(int);
	void setAttack(int);
	void setDefence(int);
	void setGold(int);
	void setLocation(Cell *);
	void setDefaultValues();
	void resetfloor();

	int damage(int attacker, int defender);
	void affect(GameObject * target);
	bool move(std::string dir);
	bool use(std::string dir);
	void tick();

	//Needs to be overidden
	virtual bool attack(std::string dir) = 0;

};

#endif

