#include "goblin.h"

bool Goblin::goblin_hostile = true;

Goblin::Goblin(Cell * spawn_location) {
	setDisplay('g');
	setHealth(75);
	setAttack(30);
	setDefence(20);
	setName("Goblin");
	setStatus("alive");
	setLocation(spawn_location);
}

int Goblin::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}
void Goblin::affect(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The evil Goblin strikes you for " << dmg << " damage!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Goblin::use_potion(GameObject * target) {
	target->affect(this);
}

void Goblin::tick() {
	//Checking if Goblin Object is Dead.
	if (getHealth() <= 0) {
		getLocation()->removeObject();
		setStatus("dead");
		new Gold(getLocation());
		std::cout << "The Goblin has died." << std::endl;
		return;
	}

	std::vector<Cell*> neighbours = getLocation()->getNeighbours();
	bool is_player_nearby = false;
	bool is_potion_nearby = false;
	Cell * target_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			target_location = neighbours.at(i);
			break;
		}
		if (neighbours.at(i)->get_contents_name() == "potion") {
			is_potion_nearby = true;
			target_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby && goblin_hostile) {
		affect(target_location->getContents());
	}
	else if (is_potion_nearby) {
		GameObject * potion = target_location->getContents();
		potion->affect(this);
		target_location->removeObject();
		delete potion;
	}
	else {
		if (!getStopwander()) {
			std::vector<Cell *> moveable_spots = getLocation()->findFreeCell();
			if (moveable_spots.size() != 0) {
				int direction = prng(moveable_spots.size() - 1);
				Cell * newlocation = moveable_spots.at(direction);
				getLocation()->removeObject();
				setLocation(newlocation);
			}
		}
	}
}

void Goblin::turnhostile() {
	if (!goblin_hostile) {
		std::cout << "This is an act of war to every Goblin!" << std::endl;
		goblin_hostile = true;
	}
}
void Goblin::turn_off_hostile() {
	goblin_hostile = false;
}