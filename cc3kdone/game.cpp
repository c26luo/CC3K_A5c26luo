#include "game.h"
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// THESE ARE PUBLIC FUNCTIONS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Game::Game(ifstream & infile) {
	stopdeath = false;
	turns = 0;
	width = 79;
	height = 25;
	floor = 1;
	theDisplay = new Display(width, height);
	string line;
	for (int i = 0; i < height; ++i) {
		getline(infile, line);
		theDisplay->addLine(line);
	}
	// Block : Setting Cells into Maps
	initRooms(3, 3, Room0);
	initRooms(4, 41, Room1);
	initRooms(10, 38, Room2);
	initRooms(15, 4, Room3);
	initRooms(16, 65, Room4);
	initPassage(4, 29, Passage);
	initPassage(14, 69, Passage);
	initNeighbours();
	sort_cell_neighbours();
}

Game::~Game() {
	delete theDisplay;
}

void Game::print() {
	theDisplay->print();
	// NEEDS TO BE EDITTED TO MATCH OUTPUT
	cout << "     " << "HP: " << player->getHealth() << "/" << player->getMaxHP() << "     " << "DEF: " << player->getDefence() << endl;
	cout << "     " << "Turns: " << getTurns() << "        " << "ATK: " << player->getAttack() << "     " << "Gold: " << player->getGold() << endl;
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Test and Initialization Functions
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// This is a Test function.
Display * Game::getDisplay() {
	return theDisplay;
}
// This is a Test function.
void Game::findFreeCells(int x, int y) {
	if (AllCells.find(make_pair(x, y)) == AllCells.end()) {
		cout << "The given coordinate does not contain a movable cell" << endl;
		return;
	}
	Cell * temp = AllCells.find(make_pair(x, y))->second;
	vector<Cell *> result = temp->findFreeCell();
	for (unsigned int i = 0; i < result.size(); ++i) {
		cout << "(" << result[i]->x() << "," << result[i]->y() << ") ";
	}
	cout << endl;
}

void Game::initRooms(int x, int y, map< pair<int, int>, Cell *> &room) {
	if (theDisplay->display_at(x, y) == '.') {
		map< pair<int, int>, Cell *>::iterator it = room.find(make_pair(x,y));
		if ( it == room.end() ) {
			Cell * newcell = new Cell(x, y, '.', true);
			room[make_pair(x,y)] = newcell;
			AllCells[make_pair(x, y)] = newcell;
			// Up
			initRooms(x-1, y, room);
			// Down
			initRooms(x+1, y, room);
			// Left
			initRooms(x, y-1, room);
			// Right
			initRooms(x, y+1, room);
		}
	}
}

void Game::initPassage(int x, int y, map< pair<int, int>, Cell *> &room) {
	char c = theDisplay->display_at(x, y);
	if ( c == '+' || c == '#') {
		map< pair<int, int>, Cell *>::iterator it = room.find(make_pair(x, y));
		if (it == room.end()) {
			Cell * newcell = new Cell(x, y, c, false);
			newcell->addDisplay(theDisplay);
			room[make_pair(x, y)] = newcell;
			AllCells[make_pair(x, y)] = newcell;
			// Up
			initPassage(x - 1, y, room);
			// Down
			initPassage(x + 1, y, room);
			// Left
			initPassage(x, y - 1, room);
			// Right
			initPassage(x, y + 1, room);
		}
	}
}

void Game::initNeighbours() {
	for (map <pair<int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		int x = it->first.first;
		int y = it->first.second;
		if (AllCells.find(make_pair(x - 1, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y - 1)]);
		}
		if (AllCells.find(make_pair(x - 1, y)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y)]);
		}
		if (AllCells.find(make_pair(x - 1, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y + 1)]);
		}
		if (AllCells.find(make_pair(x, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x, y - 1)]);
		}
		if (AllCells.find(make_pair(x, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x, y + 1)]);
		}
		if (AllCells.find(make_pair(x + 1, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y - 1)]);
		}
		if (AllCells.find(make_pair(x  + 1, y)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y)]);
		}
		if (AllCells.find(make_pair(x + 1, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y + 1)]);
		}
	}
}

void Game::sort_cell_neighbours() {
	for (map< pair<int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		vector<Cell *> some_cells_neighbours = it->second->getNeighbours();
		std::sort(some_cells_neighbours.begin(), some_cells_neighbours.end(), cell_comp_fn);
	}
}

bool Game::cell_comp_fn(Cell * a, Cell * b) {
	int ax = a->x();
	int ay = a->y();
	int bx = b->x();
	int by = b->y();

	if (ax < bx) { return true; }
	if (ax > bx) { return false; }
	if (ax == bx) {
		if (ay < by) { return true; }
		if (ay > by) { return false; }
	}
	return true;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Play function and Command Interpertr Functions
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void Game::play() {
	start:
	string s;
	cout << "Welcome to ChamberCrawler3000!" << endl;
	cout << "What would you like to play as today?" << endl;
	cout << "w) Wizard" << endl;
	cout << "k) Knight" << endl;
	cout << "s) Samurai" << endl;
	while (true) {
		cin >> s;
		if (cin.eof()) {
			break;
		}
		if (s == "k") {
			cout << "You have chosen to play as a Knight. Good luck." << endl;
			player = new Knight();
			break;
		}
		if (s == "s") {
			cout << "You have chosen to play as a Samurai. Good luck." << endl;
			player = new Samurai();
			Orc::turn_off_hostile();
			GridBug::turn_off_hostile();
			Goblin::turn_off_hostile();
			Dragon::turn_off_hostile();
			break;
		}
		if (s == "w") {
			cout << "You have chosen to play as a Wizard. Good luck." << endl;
			player = new Wizard();
			break;
		}
		else {
			cout << "Did not recognize input." << endl;
		}
	}
	spawn_map();
	print();
	// Command Interperter:
	while (1) {
		cout << "What will you do?" << endl;
		cin >> s;
		if (cin.eof()) {
			break;
		}
		if (s == "r") {
			cout << "Are you sure you want to restart? (y/n)" << endl;
			cin >> s;
			if (s == "y") {
				wipegame();
				goto start;
			}
			print();
		}
		else if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
			if (move(s)) {
				if (floor > 5) {
					cout << "At long last, you have outmatched the Great Cavernous Chambers. Great things await you." << endl;
					cout << "You achieved a score of " << player->getGold() << "." << endl;
					break;
				}
				if (getHP() <= 0 && !stopdeath ) {
					cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
					cout << "You achieved a score of " << player->getGold() << "." << endl;
					cout << "Play again? (y/n)" << endl;
					cin >> s;
					if (s == "y") {
						wipegame();
						goto start;
					}
					cout << "You have chosen to exit. At least you tried." << endl;
					break;
				}
				print();
			}
		}
		else if (s == "u") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				if (command_u(s)) {
					if (getHP() <= 0 && !stopdeath) {
						cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
						cout << "You achieved a score of " << player->getGold() << "." << endl;
						cout << "Play again? (y/n)" << endl;
						cin >> s;
						if (s == "y") {
							wipegame();
							goto start;
						}
						cout << "You have chosen to exit. At least you tried." << endl;
						break;
					}
					print();
				}
			}
			else {
				cout << "Did not recognize input." << endl;
			}
		}
		else if (s == "a") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				if (command_a(s)) {
					if (getHP() <= 0 && !stopdeath ) {
						cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
						cout << "You achieved a score of " << player->getGold() << "." << endl;
						cout << "Play again? (y/n)" << endl;
						cin >> s;
						if (s == "y") {
							wipegame();
							goto start;
						}
						cout << "You have chosen to exit. At least you tried." << endl;
						break;
					}
					print();
				}
			}
			else {
				cout << "Did not recognize input." << endl;
			}
		}
		else if (s == "q") {
			cout << "Are you sure you want to quit? (y/n)" << endl;
			cin >> s;
			if (s == "y") {
				cout << "You have chosen to exit. At least you tried." << endl;
				break;
			}
			print();
		}
		else if (s == "stopwander") {
			Monster_List.at(0)->stopmovement();
			cout << "Enemy actions that would result in movement now do nothing instead." << endl;
			print();
		}
		else if (s == "stopdeath") {
			stopdeath = true;
			cout << "Player death can no longer occur." << endl;
			print();
		}
		else {
			cout << "Did not recognize input." << endl;
		}
	}
}
void Game::play(ifstream & map) {
start:
	string s;
	cout << "Welcome to ChamberCrawler3000!" << endl;
	cout << "What would you like to play as today?" << endl;
	cout << "w) Wizard" << endl;
	cout << "k) Knight" << endl;
	cout << "s) Samurai" << endl;
	while (true) {
		cin >> s;
		if (cin.eof()) {
			break;
		}
		if (s == "k") {
			cout << "You have chosen to play as a Knight. Good luck." << endl;
			player = new Knight();
			break;
		}
		if (s == "s") {
			cout << "You have chosen to play as a Samurai. Good luck." << endl;
			player = new Samurai();
			Orc::turn_off_hostile();
			GridBug::turn_off_hostile();
			Goblin::turn_off_hostile();
			Dragon::turn_off_hostile();
			break;
		}
		if (s == "w") {
			cout << "You have chosen to play as a Wizard. Good luck." << endl;
			player = new Wizard();
			break;
		}
		else {
			cout << "Did not recognize input." << endl;
		}
	}
	spawn_map(map);
	print();
	// Command Interperter:
	while (1) {
		cout << "What will you do?" << endl;
		cin >> s;
		if (cin.eof()) {
			break;
		}
		if (s == "r") {
			cout << "Are you sure you want to restart? (y/n)" << endl;
			cin >> s;
			if (s == "y") {
				wipegame();
				goto start;
			}
			print();
		}
		else if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
			if (move(s, map)) {
				if (floor > 5) {
					cout << "At long last, you have outmatched the Great Cavernous Chambers. Great things await you." << endl;
					cout << "You achieved a score of " << player->getGold() << "." << endl;
					break;
				}
				if (getHP() <= 0 && !stopdeath) {
					cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
					cout << "You achieved a score of " << player->getGold() << "." << endl;
					cout << "Play again? (y/n)" << endl;
					cin >> s;
					if (s == "y") {
						wipegame();
						goto start;
					}
					cout << "You have chosen to exit. At least you tried." << endl;
					break;
				}
				print();
			}
		}
		else if (s == "u") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				if (command_u(s)) {
					if (getHP() <= 0 && !stopdeath) {
						cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
						cout << "You achieved a score of " << player->getGold() << "." << endl;
						cout << "Play again? (y/n)" << endl;
						cin >> s;
						if (s == "y") {
							wipegame();
							goto start;
						}
						cout << "You have chosen to exit. At least you tried." << endl;
						break;
					}
					print();
				}
			}
			else {
				cout << "Did not recognize input." << endl;
			}
		}
		else if (s == "a") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				if (command_a(s)) {
					if (getHP() <= 0 && !stopdeath) {
						cout << "You have been bested by the Great Cavernous Chambers. Good luck next time!" << endl;
						cout << "You achieved a score of " << player->getGold() << "." << endl;
						cout << "Play again? (y/n)" << endl;
						cin >> s;
						if (s == "y") {
							wipegame();
							goto start;
						}
						cout << "You have chosen to exit. At least you tried." << endl;
						break;
					}
					print();
				}
			}
			else {
				cout << "Did not recognize input." << endl;
			}
		}
		else if (s == "q") {
			cout << "Are you sure you want to quit? (y/n)" << endl;
			cin >> s;
			if (s == "y") {
				cout << "You have chosen to exit. At least you tried." << endl;
				break;
			}
			print();
		}
		else if (s == "stopwander") {
			Monster_List.at(0)->stopmovement();
			cout << "Enemy actions that would result in movement now do nothing instead." << endl;
			print();
		}
		else if (s == "stopdeath") {
			stopdeath = true;
			cout << "Player death can no longer occur." << endl;
			print();
		}
		else {
			cout << "Did not recognize input." << endl;
		}
	}

}

bool Game::move(string direction) {
	if (player->move(direction)) {
		if (player->getDescending()) {
			resetfloor();
			return true;
		}
		tick();
		return true;
	}
	return false;
}

bool Game::move(string direction, ifstream & map) {
	if (player->move(direction)) {
		if (player->getDescending()) {
			resetfloor(map);
			return true;
		}
		tick();
		return true;
	}
	return false;
}

bool Game::command_a(string dir) {
	if (player->attack(dir)) {
		tick();
		return true;
	}
	return false;
}

bool Game::command_u(string dir) {
	if (player->use(dir)) {
		tick();
		return true;
	}
	return false;
}

void Game::tick() {
	turns++;
	player->tick();
	sort_monsterlist();
	for (unsigned int i = 0; i < Monster_List.size(); ++i) {
		Monster_List.at(i)->tick();
	}
	for (unsigned int i = 0; i < Monster_List.size(); ++i) {
		if (Monster_List.at(i)->getStatus() == "dead") {
			delete Monster_List.at(i);
			Monster_List.erase(Monster_List.begin() + i);
		}
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Helper function used by Play and Command functions
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int Game::getTurns() {
	return turns;
}
int Game::getHP() {
	return player->getHealth();
}

void Game::spawn_map(ifstream & map) {
	theDisplay->getDisplay().clear();
	string line;
	for (int i = 0; i < height; ++i) {
		getline(map, line);
		theDisplay->addLine(line);
	}

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			Cell * spawn_location = 0;
			char temp = theDisplay->display_at(i, j);
			if (temp == 'X') { 
				spawn_location = AllCells[make_pair(i, j)];
				Monster * monster = new GridBug(spawn_location);
				Monster_List.push_back(monster);
			}
			if (temp == 'g') {
				spawn_location = AllCells[make_pair(i, j)];
				Monster * monster = new Goblin(spawn_location);
				Monster_List.push_back(monster);
			}
			if (temp == 'M') {
				spawn_location = AllCells[make_pair(i, j)];
				Monster * monster = new Merchant(spawn_location);
				Monster_List.push_back(monster);
			}
			if (temp == 'O') {
				spawn_location = AllCells[make_pair(i, j)];
				Monster * monster = new Orc(spawn_location);
				Monster_List.push_back(monster);
			}
			if (temp == '0') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(0, spawn_location);
			}
			if (temp == '1') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(1, spawn_location);
			}
			if (temp == '2') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(2, spawn_location);
			}
			if (temp == '3') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(3, spawn_location);
			}
			if (temp == '4') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(4, spawn_location);
			}
			if (temp == '5') {
				spawn_location = AllCells[make_pair(i, j)];
				new Potion(5, spawn_location);
			}
			if (temp == '6') {
				spawn_location = AllCells[make_pair(i, j)];
				new Gold(spawn_location);
			}
			if (temp == '7') {
				spawn_location = AllCells[make_pair(i, j)];
				DragonHoard * hoard = new DragonHoard(spawn_location);
				if (theDisplay->display_at(i-1, j-1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i - 1, j - 1)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i-1, j) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i - 1, j)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i-1, j+1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i - 1, j + 1)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i, j-1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i, j - 1)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i, j+1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i, j + 1)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i+1, j-1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i + 1, j - 1)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i+1, j) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i + 1, j)];
					new Dragon(dragon_location, hoard);
				}
				if (theDisplay->display_at(i+1, j+1) == 'D') {
					Cell * dragon_location = AllCells[make_pair(i + 1, j + 1)];
					new Dragon(dragon_location, hoard);
				}
			}
			if (temp == '@'){
				spawn_location = AllCells[make_pair(i, j)];
				player->setLocation(spawn_location);
				spawn_location->setContent(player);
				spawn_location->setDisplay('@');
			}
			if (temp == '>') {
				spawn_location = AllCells[make_pair(i, j)];
				new Stairs(spawn_location);
			}
 		}
	}
}

void Game::spawn_map() {
	int NUM_POTIONS = 10;
	int NUM_GOLD = 10;
	int NUM_MONSTERS = 20;
	int player_chamber = prng(4);
	spawnplayer(player_chamber);
	spawnstairs(player_chamber);
	for (int i = 0; i < NUM_POTIONS; ++i) {
		int potion_type = prng(5);
		spawnpotion(potion_type);
	}
	for (int i = 0; i < NUM_GOLD; ++i) {
		int gold_type = prng(7);
		spawngold(gold_type);
	}
	for (int i = 0; i < NUM_MONSTERS; ++i) {
		int monster_type = prng(5); // 5 beacuse [X,X,g,g,O,M]
		spawnmonster(monster_type);
	}
}

void Game::spawnplayer(int chamber) {
	while (true) {
		int cell;
		Cell * spawn_location = Room0.begin()->second;
		if (chamber == 0) {
			cell = prng(Room0.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 1) {
			cell = prng(Room1.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 2) {
			cell = prng(Room2.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 3) {
			cell = prng(Room3.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 4) {
			cell = prng(Room4.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		player->setLocation(spawn_location);
		spawn_location->setContent(player);
		spawn_location->setDisplay('@');
		break;
	}
}

void Game::spawnmonster(int type) {
	Cell * spawn_location = Room0.begin()->second;
	while (true) {
		int chamber = prng(4);
		int cell;
		if (chamber == 0) {
			cell = prng(Room0.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 1) {
			cell = prng(Room1.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 2) {
			cell = prng(Room2.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 3) {
			cell = prng(Room3.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 4) {
			cell = prng(Room4.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		// Checking if the cell is valid
		if (spawn_location->isEmpty()) { break; }
	}
	if (type == 0 || type == 1) {
		Monster * monster = new GridBug(spawn_location);
		Monster_List.push_back(monster);
	}
	if (type == 2 || type == 3) {
		Monster * monster = new Goblin(spawn_location);
		Monster_List.push_back(monster);
	}
	if (type == 4) {
		Monster * monster = new Orc(spawn_location);
		Monster_List.push_back(monster);
	}
	if (type == 5) {
		Monster * monster = new Merchant(spawn_location);
		Monster_List.push_back(monster);
	}
}

void Game::spawnpotion(int potion_type) {
	Cell * spawn_location = Room0.begin()->second;
	while (true) {
		int chamber = prng(4);
		int cell;
		if (chamber == 0) {
			cell = prng(Room0.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 1) {
			cell = prng(Room1.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 2) {
			cell = prng(Room2.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 3) {
			cell = prng(Room3.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 4) {
			cell = prng(Room4.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		// Checking if the cell is valid
		if (spawn_location->isEmpty()) { break; }
	}
	if( potion_type == 0) { new Potion(0, spawn_location); }
	if (potion_type == 1) { new Potion(1, spawn_location); }
	if (potion_type == 2) { new Potion(2, spawn_location); }
	if (potion_type == 3) { new Potion(3, spawn_location); }
	if (potion_type == 4) { new Potion(4, spawn_location); }
	if (potion_type == 5) { new Potion(5, spawn_location); }
}

void Game::spawngold(int gold_type) {
	Cell * spawn_location = Room0.begin()->second;
	while (true) {
		reroll:
		int chamber = prng(4);
		int cell;
		if (chamber == 0) {
			cell = prng(Room0.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 1) {
			cell = prng(Room1.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 2) {
			cell = prng(Room2.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 3) {
			cell = prng(Room3.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 4) {
			cell = prng(Room4.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		// Checking if the cell is valid
		if (spawn_location->isEmpty()) { break; }
	}
	if (gold_type == 7) {
		vector<Cell *> neighbours = spawn_location->getNeighbours();
		vector<Cell *> freeCells;
		for (unsigned int i = 0; i < neighbours.size(); ++i) {
			if (neighbours.at(i)->isEmpty() ) {
				freeCells.push_back(neighbours.at(i));
			}
		}
		if (freeCells.size()) {
			DragonHoard * dragonhoard = new DragonHoard(spawn_location);
			Monster * dragon = new Dragon(freeCells.at(prng(freeCells.size() - 1)), dragonhoard);
			Monster_List.push_back(dragon);
		}
		else {
			goto reroll;
		}
	}
	else {
		new Gold(spawn_location);
	}
}

void Game::spawnstairs(int player_chamber) {
	while (true) {
		int chamber = prng(3);
		if (chamber >= player_chamber) {
			chamber += 1;
		}
		int cell = 0;
		Cell * spawn_location = 0;
		if (chamber == 0) {
			cell = prng(Room0.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 1) {
			cell = prng(Room1.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 2) {
			cell = prng(Room2.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 3) {
			cell = prng(Room3.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		if (chamber == 4) {
			cell = prng(Room4.size() - 1);
			for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
				if (cell == 0) {
					spawn_location = it->second;
					break;
				}
				cell--;
			}
		}
		new Stairs(spawn_location);
		break;
	}
}

void Game::resetfloor() {
	//remove all gameobjects from map.
	for (map<pair <int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		if (!it->second->isEmpty()) {
			if (it->second->get_contents_name() == "player") {
				it->second->removeObject();
			}
			else {
				delete it->second->getContents();
				it->second->removeObject();
			}
		}
	}
	//empty out monster list
	Monster_List.clear();
	player->resetfloor();
	floor++;
	cout << "You have descended down to floor " << floor << "." << endl;
	spawn_map();
}

void Game::resetfloor(ifstream & inmap) {
	//remove all gameobjects from map.
	for (map<pair <int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		if (!it->second->isEmpty()) {
			if (it->second->get_contents_name() == "player") {
				it->second->removeObject();
			}
			else {
				delete it->second->getContents();
				it->second->removeObject();
			}
		}
	}
	//empty out monster list
	Monster_List.clear();
	player->resetfloor();
	floor++;
	if (floor > 5) {
		return;
	}
	cout << "You have descended down to floor " << floor << "." << endl;
	spawn_map(inmap);
}

void Game::wipegame() {
	for (map<pair <int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		if (!it->second->isEmpty()) {
			delete it->second->getContents();
			it->second->removeObject();
		}
	}
	//empty out monster list
	Monster_List.clear();
	floor = 1;
	prng.seed(217);
}

// Sort Monster List Function
bool Game::monster_comp_fn(Monster * a, Monster * b) {
	int ax = a->getLocation()->x();
	int ay = a->getLocation()->y();
	int bx = b->getLocation()->x();
	int by = b->getLocation()->y();

	if (ax < bx) { return true; }
	if (ax > bx) {
		return false;
	}
	if (ax == bx) {
		if (ay < by) { return true; }
		if (ay > by) { return false; }
	}
	// This case should never be excuted. 
	return true;
}
void Game::sort_monsterlist() {
	std::sort(Monster_List.begin(), Monster_List.end(), monster_comp_fn);
}








