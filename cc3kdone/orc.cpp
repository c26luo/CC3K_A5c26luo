#include "orc.h"

bool Orc::orc_hostile = true;

Orc::Orc(Cell * spawn_location) {
	setDisplay('O');
	setHealth(120);
	setAttack(30);
	setDefence(30);
	setName("Orc");
	setStatus("alive");
	setLocation(spawn_location);
}

int Orc::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}

void Orc::affect(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The evil Orc strikes you for " << dmg << " damage!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Orc::tick() {
	if (getHealth() <= 0) {
		getLocation()->removeObject();
		setStatus("dead");
		new Gold(getLocation());
		std::cout << "The Orc has died." << std::endl;
		return;
	}
	std::vector<Cell*> neighbours = getLocation()->getNeighbours();
	bool is_player_nearby = false;
	Cell * player_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			player_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby && orc_hostile) {
		affect(player_location->getContents());
	}
	else {
		if (!getStopwander()) {
			std::vector<Cell *> moveable_spots = getLocation()->findFreeCell();
			if (moveable_spots.size() != 0) {
				int direction = prng(moveable_spots.size() - 1);
				Cell * newlocation = moveable_spots.at(direction);
				getLocation()->removeObject();
				setLocation(newlocation);
			}
		}
	}
}

void Orc::turnhostile() {
	if (!orc_hostile) {
		std::cout << "This is an act of war to every Orc!" << std::endl;
		orc_hostile = true;
	}
}
void Orc::turn_off_hostile() {
	orc_hostile = false;
}