#include "potion.h"

Potion::Potion(int type, Cell * spawn_location) {
	_display = '!';
	name = "potion";
	if (type == 0) {
		// Restore Health RH increase by 30
		stat_value = 30;
		potion_type = "RH";
	}
	if (type == 1) {
		// Boost Atk BA increase ATK by 10
		stat_value = 10;
		potion_type = "BA";
	}
	if (type == 2) {
		// Boost Def BD increase Def by 10
		stat_value = 10;
		potion_type = "BD";
	}
	if (type == 3) {
		//Poison health PH decrease HP by 15
		stat_value = -15;
		potion_type = "PH";
	}
	if (type == 4) {
		//Wound Atk WA decrease Atk by 5
		stat_value = -5;
		potion_type = "WA";
	}
	if (type == 5) {
		//Wound Def WD decrease Def by 5
		stat_value = -5;
		potion_type = "WD";
	}
	spawn_location->setContent(this);
	spawn_location->setDisplay(_display);
}

void Potion::affect(GameObject * thing) {
	std::string name = thing->getName();
	if (potion_type == "RH") {
		if (name == "player") {
			std::cout << "You chug the Restore Health potion." << std::endl;
		}
		thing->setHealth(thing->getHealth() + stat_value);
	}
	if (potion_type == "BA") {
		if (name == "player") {
			std::cout << "You chug the Boost Attack potion." << std::endl;
		}
		thing->setAttack(thing->getAttack() + stat_value);
	}
	if (potion_type == "BD") {
		if (name == "player") {
			std::cout << "You chug the Boost Defence potion." << std::endl;
		}
		thing->setDefence(thing->getDefence() + stat_value);
	}
	if (potion_type == "PH") {
		if (name == "player") {
			std::cout << "You chug the Poison Health potion." << std::endl;
		}
		thing->setHealth(thing->getHealth() + stat_value);
	}
	if (potion_type == "WA") {
		if (name == "player") {
			std::cout << "You chug the Wound Attack potion." << std::endl;
		}
		thing->setAttack(thing->getAttack() + stat_value);
	}
	if (potion_type == "WD") {
		if (name == "player") {
			std::cout << "You chug the Wound Defence potion." << std::endl;
		}
		thing->setDefence(thing->getDefence() + stat_value);
	}
}
std::string Potion::getName() {
	return name;
}
std::string Potion::getStatus() {
	return potion_type;
}