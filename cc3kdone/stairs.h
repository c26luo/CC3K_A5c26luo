#ifndef STAIRS_H
#define STAIRS_H
#include "game_object.h"

class Stairs : public GameObject {
	std::string name;
	char _display;
public:
	Stairs(Cell *);
	std::string getName();

	std::string getStatus() { return ""; }
	int getAttack() { return 0; }
	int getDefence() { return 0; }
	int getHealth() { return 0; }
	void setHealth(int) {}
	void setAttack(int) {}
	void setDefence(int) {}
	void affect(GameObject *) {}
	void tick() {}
};

#endif