#include "player.h"

// Get
int Character::getHealth() {
	return _hp;
}

int Character::getMaxHP() {
	return maxhp;
}

int Character::getAttack() {
	return _att;
}

std::string Character::getStatus() {
	return status;
}

int Character::getDefence() {
	return _def;
}
std::string Character::getName() {
	return name;
}
Cell * Character::getLocation() {
	return location;
}
int Character::getGold() {
	return gold;
}
bool Character::getDescending() {
	return is_descending;
}

//Set
void Character::setHealth(int n){
	if (n < 0) {
		_hp = 0;
	}
	else {
		_hp = n;
	}
}
void Character::setAttack(int n){
	_att = n;
}
void Character::setDefence(int n){
	_def = n;
}
void Character::setGold(int n) {
	gold = n;
}

void Character::setDefaultValues() {
	name = "player";
	status = "alive";
	maxhp = _hp;
	original_att = _att;
	original_def = _def;
	is_descending = false;
}

void Character::resetfloor() {
	_att = original_att;
	_def = original_def;
	is_descending = false;
}

void Character::setLocation(Cell * spawn_location) {
	location = spawn_location;
}

int Character::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}

bool Character::move(std::string dir) {
	std::vector<Cell *> neighbour = location->getNeighbours();
	int index = 246;
	int x = location->x();
	int y = location->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}

	if (index == 246) {
		std::cout << "You can't move in that direction." << std::endl;
		return false;
	}

	Cell * newlocation = neighbour.at(index);

	if (newlocation->isEmpty()) {
		newlocation->setContent(this);
		newlocation->setDisplay('@');
		location->removeObject();
		location = newlocation;
		if (dir == "nw") { std::cout << "You moved north-west." << std::endl; }
		if (dir == "no") { std::cout << "You moved north." << std::endl; }
		if (dir == "ne") { std::cout << "You moved north-east." << std::endl; }
		if (dir == "we") { std::cout << "You moved west." << std::endl; }
		if (dir == "ea") { std::cout << "You moved east." << std::endl; }
		if (dir == "so") { std::cout << "You moved south." << std::endl; }
		if (dir == "sw") { std::cout << "You moved south-west." << std::endl; }
		if (dir == "se") { std::cout << "You moved south-east." << std::endl; }
	}
	else if (newlocation->get_contents_name() == "gold pile") {
		GameObject * temp = newlocation->getContents();
		newlocation->removeObject();
		delete temp;
		gold += 10;
		std::cout << "You pick up the Gold Pile worth 10gp." << std::endl;
	}
	else if (newlocation->get_contents_name() == "dragon hoard") {
		std::cout << "It would be foolhardy to take the dragon's gold right now." << std::endl;
	}
	else if (newlocation->get_contents_name() == "pickup-able hoard") {
		GameObject * temp = newlocation->getContents();
		newlocation->removeObject();
		delete temp;
		gold += 50;
		std::cout << "You successfully take the Dragon Hoard!" << std::endl;
	}
	else if (newlocation->get_contents_name() == "stairs") {
		is_descending = true;
		return true;
	}
	else {
		std::cout << "You can't move in that direction." << std::endl;
		return false;
	}
	return true;
}

bool Character::use(std::string dir) {
	std::vector<Cell *> neighbour = location->getNeighbours();
	int index = 246;
	int x = location->x();
	int y = location->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}
	if (index == 246) {
		std::cout << "There is nothing there to use!" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "nothing") {
		std::cout << "Nothing to Use" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "gold pile") {
		GameObject * gold = neighbour.at(index)->getContents();
		neighbour.at(index)->removeObject();
		delete gold;
		gold += 10;
		std::cout << "You pick up the Gold Pile worth 10gp." << std::endl;
	}
	else if (neighbour.at(index)->get_contents_name() == "dragon hoard") {
		std::cout << "It would be foolhardy to take the dragon's gold right now." << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "pickup-able hoard") {
		GameObject * hoard = neighbour.at(index)->getContents();
		neighbour.at(index)->removeObject();
		delete hoard;
		gold += 50;
		std::cout << "You successfully take the Dragon Hoard!" << std::endl;
	}
	else if (neighbour.at(index)->get_contents_name() == "potion") {
		GameObject* potion = neighbour.at(index)->getContents();
		potion->affect(this);
		neighbour.at(index)->removeObject();
		delete potion;
	}
	else {
		std::cout << "There is nothing there to use!" << std::endl;
		return false;
	}
	return true;
}

void Character::affect(GameObject * target) {
	int dmg = damage(_att, target->getDefence());
	target->setHealth(target->getHealth() - dmg);
}

void Character::tick() {
	if (_hp == maxhp) {
		return;
	}
	else if (_hp + 5 >= maxhp) {
		_hp = maxhp;
	}
	else {
		_hp += 5;
	}
}
