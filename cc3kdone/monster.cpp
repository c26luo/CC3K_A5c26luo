#include "monster.h"
// Static Decleration
bool Monster::stopwander = false;

// Get
int Monster::getHealth() {
	return _hp;
}
int Monster::getAttack() {
	return _att;
}
int Monster::getDefence() {
	return _def;
}
bool Monster::getStopwander() {
	return stopwander;
}
std::string Monster::getName() {
	return name;
}
std::string Monster::getStatus() {
	return status;
}
Cell * Monster::getLocation() {
	return location;
}
char Monster::getDisplay() {
	return _display;
}

//Set
void Monster::setHealth(int hp) {
	_hp = hp;
}
void Monster::setAttack(int att) {
	_att = att;
}
void Monster::setDefence(int def) {
	_def = def;
}
void Monster::setName(std::string s) {
	name = s;
}
void Monster::setStatus(std::string s) {
	status = s;
}

void Monster::stopmovement() {
	stopwander = true;
}

void Monster::setLocation(Cell * spawn_location) {
	location = spawn_location;
	location->setContent(this);
	location->setDisplay(_display);
}

void Monster::setDisplay(char img) {
	_display = img;
}