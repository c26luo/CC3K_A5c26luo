#include "gridbug.h"

bool GridBug::gridbug_hostile = true;

GridBug::GridBug(Cell * spawn_location) {
	setDisplay('X');
	setHealth(50);
	setAttack(20);
	setDefence(10);
	setName("Grid Bug");
	setStatus("alive");
	setLocation(spawn_location);
}

int GridBug::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}
void GridBug::affect(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The evil Grid Bug strikes you for " << dmg << " damage!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void GridBug::tick() {
	//Checking if Goblin Object is Dead.
	if (getHealth() <= 0) {
		getLocation()->removeObject();
		setStatus("dead");
		new Potion(prng(5), getLocation());
		std::cout << "The Grid Bug died." << std::endl;
		return;
	}
	std::vector<Cell*> neighbours = getLocation()->getNeighbours();
	bool is_player_nearby = false;
	Cell * target_location = 0;
	int x = getLocation()->x();
	int y = getLocation()->y();
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			int neighbour_x = neighbours.at(i)->x();
			int neighbour_y = neighbours.at(i)->y();
			if (x - 1 == neighbour_x && y == neighbour_y ) {
				is_player_nearby = true;
				target_location = neighbours.at(i);
				break;
			}
			if (x + 1 == neighbour_x && y == neighbour_y) {
				is_player_nearby = true;
				target_location = neighbours.at(i);
				break;
			}
			if (x == neighbour_x && y - 1 == neighbour_y) {
				is_player_nearby = true;
				target_location = neighbours.at(i);
				break;
			}
			if (x == neighbour_x && y + 1 == neighbour_y) {
				is_player_nearby = true;
				target_location = neighbours.at(i);
				break;
			}
		}
	}
	if (is_player_nearby && gridbug_hostile) {
		affect(target_location->getContents());
	}
	else {
		if (!getStopwander()) {
			std::vector<Cell *> free_cells_in_1_block_radius = getLocation()->findFreeCell();
			std::vector<Cell *> moveable_spots;
			for (unsigned int i = 0; i < free_cells_in_1_block_radius.size(); ++i) {
				int neighbour_x = free_cells_in_1_block_radius.at(i)->x();
				int neighbour_y = free_cells_in_1_block_radius.at(i)->y();
				if (x - 1 == neighbour_x && y == neighbour_y) {
					moveable_spots.push_back(free_cells_in_1_block_radius.at(i));
				}
				else if (x == neighbour_x && y - 1== neighbour_y) {
					moveable_spots.push_back(free_cells_in_1_block_radius.at(i));
				}
				else if (x == neighbour_x && y + 1 == neighbour_y) {
					moveable_spots.push_back(free_cells_in_1_block_radius.at(i));
				}
				else if (x + 1 == neighbour_x && y == neighbour_y) {
					moveable_spots.push_back(free_cells_in_1_block_radius.at(i));
				}
			}
			if (moveable_spots.size() != 0) {
				int direction = prng(moveable_spots.size() - 1);
				Cell * newlocation = moveable_spots.at(direction);
				getLocation()->removeObject();
				setLocation(newlocation);
			}
		}
	}
}

void GridBug::turnhostile() {
	if (!gridbug_hostile) {
		std::cout << "This is an act of war to every Grid Bug!" << std::endl;
		gridbug_hostile = true;
	}
}
void GridBug::turn_off_hostile() {
	gridbug_hostile = false;
}