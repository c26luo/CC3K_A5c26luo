#ifndef KNIGHT_H
#define KNIGHT_H
#include "player.h"

class Knight : public Character {
public:
	Knight();
	bool attack(std::string dir);
};


#endif