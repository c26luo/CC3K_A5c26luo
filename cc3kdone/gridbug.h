#ifndef GRIDBUG_H
#define GRIDBUG_H
#include "monster.h"
#include "potion.h"

class GridBug : public Monster {
	static bool gridbug_hostile;
public:
	GridBug(Cell * spawn_location);
	int damage(int attker, int defender);
	void affect(GameObject * target);
	void tick();
	static void turnhostile();
	static void turn_off_hostile();
};

#endif
