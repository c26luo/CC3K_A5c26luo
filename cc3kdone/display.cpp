#include "display.h"

Display::Display(int width, int height) : width(width), height(height) {
}

Display::~Display() {
}

void Display::notify(int x, int y, char ch) {
	boardDisplay[x][y] = ch;
}

void Display::print() {
	for (std::vector<std::string>::iterator it = boardDisplay.begin(); it != boardDisplay.end(); ++it) {
		std::cout << *it << std::endl;
	}
}

void Display::addLine(std::string s) {
	boardDisplay.push_back(s);
}

char Display::display_at(int x, int y) {
	return boardDisplay[x][y];
}

std::vector< std::string> & Display::getDisplay() {
	return boardDisplay;
}