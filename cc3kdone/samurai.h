#ifndef SAMURAI_H
#define SAMURAI_H
#include "player.h"

class Samurai : public Character {
public:
	Samurai();
	bool attack(std::string dir);
};

#endif