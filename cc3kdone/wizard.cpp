#include "wizard.h"
#include "merchant.h"

Wizard::Wizard() {
	setHealth(60);
	setAttack(25);
	setDefence(0);
	setGold(0);
	setDefaultValues();
}

bool Wizard::lazerbeam_attack(Cell * target, int x_dir, int y_dir) {
	if (target->get_contents_name() == "nothing") {
		std::vector<Cell *> neighbour = target->getNeighbours();
		int index = 246;
		int x = target->x() + x_dir;
		int y = target->y() + y_dir;
		for (unsigned int i = 0; i < neighbour.size(); ++i) {
			int vx = neighbour[i]->x();
			int vy = neighbour[i]->y();
			if (vx == x && vy == y) {
				index = i;
				break;
			}
		}
		if (index == 246) {
			std::cout << "There is nothing there to attack!" << std::endl;
			return false;
		}
		else {
			return lazerbeam_attack(neighbour[index], x_dir, y_dir);
		}
	}
	else if (target->get_contents_name() == "Merchant") {
		Merchant::turnhostile();
	}
	//else {
	//	std::cout << "There is nothing there to attack!" << std::endl;
	//	return false;
	//}
	affect(target->getContents());
	int dmg = damage(getAttack(), target->getContents()->getDefence());
	std::cout << "You attack the " << target->getContents()->getName();
	std::cout << " with your Infinite Loop Laser for " << dmg << " damage!" << std::endl;
	return true;
}

bool Wizard::attack(std::string dir) {
	std::vector<Cell *> neighbour = getLocation()->getNeighbours();
	int index = 246;
	int x = getLocation()->x();
	int y = getLocation()->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}
	int x_dir;
	int y_dir;
	if (dir == "nw") { x_dir = -1; y_dir = -1; }
	if (dir == "no") { x_dir = -1; y_dir = 0; }
	if (dir == "ne") { x_dir = -1; y_dir = 1; }
	if (dir == "we") { x_dir = 0; y_dir = -1; }
	if (dir == "ea") { x_dir = 0; y_dir = 1; }
	if (dir == "sw") { x_dir = 1; y_dir = -1; }
	if (dir == "so") { x_dir = 1; y_dir = 0; }
	if (dir == "se") { x_dir = 1; y_dir = 1; }
	
	if (index == 246) {
		std::cout << "There is nothing there to attack!" << std::endl;
		return false;
	}
	return lazerbeam_attack(neighbour.at(index), x_dir, y_dir);
}