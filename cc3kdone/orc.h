#ifndef ORC_H
#define ORC_H
#include "monster.h"
#include "gold.h"

class Orc : public Monster {
	static bool orc_hostile;
public:
	Orc(Cell * spawn_location);
	int damage(int attker, int defender);
	void affect(GameObject * target);
	void tick();
	static void turnhostile();
	static void turn_off_hostile();
};

#endif