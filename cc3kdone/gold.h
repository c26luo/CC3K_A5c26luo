#ifndef GOLD_H
#define GOLD_H
#include "treasure.h"

class Gold : public Treasure {
public:
	Gold(Cell * spawn_location);
};

#endif