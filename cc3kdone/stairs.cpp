#include "stairs.h"

Stairs::Stairs(Cell * spawn_location) {
	name = "stairs";
	_display = '>';
	spawn_location->setContent(this);
	spawn_location->setDisplay(_display);
}
std::string Stairs::getName() {
	return name;
}