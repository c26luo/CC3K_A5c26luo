#ifndef GOBLIN_H
#define GOBLIN_H
#include "monster.h"
#include "gold.h"

class Goblin : public Monster {
	static bool goblin_hostile;
public:
	Goblin(Cell * spawn_location);
	int damage(int attker, int defender);
	void affect(GameObject * target);
	void use_potion(GameObject * target);
	void tick();
	static void turnhostile();
	static void turn_off_hostile();
};

#endif