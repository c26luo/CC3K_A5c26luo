#include "dragon.h"

bool Dragon::dragon_hostile = true;

Dragon::Dragon(Cell * spawn_location, DragonHoard * myhoard) {
	setDisplay('D');
	setHealth(150);
	setAttack(50);
	setDefence(10);
	setName("Dragon");
	setStatus("alive");
	setLocation(spawn_location);
	hoard = myhoard;
}
int Dragon::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}
void Dragon::affect(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The evil Dragon strikes you for " << dmg << " damage!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Dragon::tick() {
	if (getHealth() <= 0) {
		getLocation()->removeObject();
		setStatus("dead");
		hoard->removeGuard();
		hoard->setName("pickup-able hoard");
		std::cout << "The Dragon has died." << std::endl;
		return;
	}

	std::vector<Cell*> neighbours = getLocation()->getNeighbours();
	bool is_player_nearby = false;
	Cell * player_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			player_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby && dragon_hostile) {
		affect(player_location->getContents());
	}
}

void Dragon::turnhostile() {
	if (!dragon_hostile) {
		std::cout << "This is an act of war to every Dragon!" << std::endl;
		dragon_hostile = true;
	}
}

void Dragon::turn_off_hostile() {
	dragon_hostile = false;
}