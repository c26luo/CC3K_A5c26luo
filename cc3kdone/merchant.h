#ifndef MERCHANT_H
#define MERCHANT_H
#include "monster.h"
#include "gold.h"

class Merchant : public Monster {
	static bool merchant_hostile;
public:
	Merchant(Cell * spawn_location);
	int damage(int attker, int defender);
	void affect(GameObject * target);
	void tick();
	static void turnhostile();
};

#endif
