#ifndef DRAGONHOARD_H
#define DRAGONHOARD_H
#include "treasure.h"

class DragonHoard : public Treasure {
	char _display;
	Cell *location;
	bool guarded;
public:
	DragonHoard(Cell *spawn_location);
	void removeGuard();
	bool isGuarded();
};

#endif
