#ifndef MONSTER_H
#define MONSTER_H
#include "game_object.h"
#include "prng.h"
#include <cmath>

extern PRNG prng;

class Monster : public GameObject {
	static bool stopwander;
	std::string name;
	std::string status;
	char _display;
	int maxhp;
	int _att;
	int _hp;
	int _def;
	Cell * location;
public:
	// Get
	int getHealth();
	int getAttack();
	int getDefence();
	bool getStopwander();
	Cell * getLocation();
	std::string getName();
	std::string getStatus();
	char getDisplay();
	//Set
	void setDisplay(char);
	void setHealth(int);
	void setAttack(int);
	void setDefence(int);
	void setName(std::string);
	void setStatus(std::string);
	void setLocation(Cell *);
	void stopmovement();

	virtual int damage(int attacker, int defender) = 0;
	virtual void affect(GameObject * target) = 0;
	virtual void tick() = 0;
};

#endif