#include "merchant.h"

bool Merchant::merchant_hostile = false;

Merchant::Merchant(Cell * spawn_location) {
	setDisplay('M');
	setHealth(100);
	setAttack(75);
	setDefence(5);
	setName("Merchant");
	setStatus("alive");
	setLocation(spawn_location);
}

int Merchant::damage(int attker, int defender) {
	double formula = (double)attker * ((100 - (double)defender) / 100);
	return ceil(formula);
}
void Merchant::affect(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The evil Merchant strikes you for " << dmg << " damage!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Merchant::tick() {
	if (getHealth() <= 0) {
		getLocation()->removeObject();
		setStatus("dead");
		new Gold(getLocation());
		std::cout << "The Merchant has died." << std::endl;
		return;
	}
	std::vector<Cell*> neighbours = getLocation()->getNeighbours();
	bool is_player_nearby = false;
	Cell * player_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			player_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby && merchant_hostile) {
		affect(player_location->getContents());
	}
	else {
		if (!getStopwander()) {
			std::vector<Cell *> moveable_spots = getLocation()->findFreeCell();
			if (moveable_spots.size() != 0) {
				int direction = prng(moveable_spots.size() - 1);
				Cell * newlocation = moveable_spots.at(direction);
				getLocation()->removeObject();
				setLocation(newlocation);
			}
		}
	}
}

void Merchant::turnhostile() {
	if (!merchant_hostile) {
		std::cout << "This is an act of war to every Merchant!" << std::endl;
		merchant_hostile = true;
	}
}