#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H
#include <string>
#include <iostream>
#include <vector>
#include "cell.h"

class GameObject {
public:
	virtual std::string getName() = 0;
	virtual std::string getStatus() = 0;
	virtual int getAttack() = 0;
	virtual int getDefence() = 0;
	virtual int getHealth() = 0;
	virtual void setHealth(int) = 0;
	virtual void setAttack(int) = 0;
	virtual void setDefence(int) = 0;
	virtual void affect(GameObject *) = 0;
	//virtual void move(std::string direction) = 0;
	virtual void tick() = 0;
};

#endif

