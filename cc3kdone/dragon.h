#ifndef DRAGON_H
#define DRAGON_H
#include "monster.h"
#include "dragonhoard.h"

class Dragon : public Monster {
	static bool dragon_hostile;
	DragonHoard * hoard;
public:
	Dragon(Cell * spawn_location, DragonHoard * hoard);
	int damage(int attker, int defender);
	void affect(GameObject * target);
	void tick();
	static void turn_off_hostile();
	static void turnhostile();
};

#endif
