#ifndef CELL_H
#define CELL_H
#include <vector>


// forward declaration because mutual reference
class GameObject;

class Cell {
	int _x, _y;
	char _display;
	GameObject *_contents;
	std::vector<Cell *> neighbours;
	bool initialized;

	// Private Methods
	bool isEmpty(); //returns if cell is empty

public:
	Cell(int x, int y, char display);
	
	// Getters
	int x(); // returns x
	int y(); // returns y
	char display(); // returns display char

	// Setter
	void setDisplay(char);
	void setContent(GameObject *);
	void removeOjbect(); // set GameObject to NULL
	void addNeighbour(Cell *); // adds a neighbour

	// vector<Cell *> findFreeCell();
		// Returns a vector of cell pointers that point to unoccupied cells.
		// Note: you can use prng( vector_name.size() - 1 ); to get moveable space

};

#endif
