#ifndef DISPLAY_H
#define DISPLAY_H
#include <iostream>
#include <vector>
#include <string>

class Display {
	std::vector< std::string > boardDisplay;
	unsigned int width;
	unsigned int height;
public:
	Display(int width, int height);
	~Display();
	char display_at(int x, int y);
	void notify(int x, int y, char ch);
	void print();
	void addLine(std::string);
};

#endif

