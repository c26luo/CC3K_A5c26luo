#include "game.h"
#include "prng.h"
//#include <process.h>
#include <unistd.h>
PRNG prng(getpid());

int main() {
	ifstream inMap("default_map.txt");
	Game game(inMap);
	game.play();
	return 0;
}
