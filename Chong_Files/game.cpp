#include "game.h"

Game::Game(ifstream & infile) {
	turns = 0;
	width = 79;
	height = 25;
	theDisplay = new Display(width, height);
	string line;
	for (int i = 0; i < height; ++i) {
		getline(infile, line);
		theDisplay->addLine(line);
	}
	// Block : Setting Cells into Maps
	initRooms(3, 3, Room0);
	initRooms(4, 41, Room1);
	initRooms(10, 38, Room2);
	initRooms(15, 4, Room3);
	initRooms(16, 65, Room4);
	initPassage(4, 29, Passage);
	initPassage(14, 69, Passage);
	initNeighbours();
	// Bloc : Room 0 - 4, AllCells, Passage are set.
}

Game::~Game() {
	delete theDisplay;
}

void Game::print() {
	theDisplay->print();
	// NEEDS TO BE EDITTED TO MATCH OUTPUT
	cout << "     " << "HP: " << player->getHealth() << "/" << player->getMaxHP() << "     " << "DEF:" << player->getDefence() << endl;
	cout << "     " << "Turns: " << getTurns() << "     " << "ATK: " << player->getAttack() << endl;
};

// This is a Test function.
Display * Game::getDisplay() {
	return theDisplay;
}
// This is a Test function.
void Game::findFreeCells(int x, int y) {
	if (AllCells.find(make_pair(x, y)) == AllCells.end()) {
		cout << "The given coordinate does not contain a movable cell" << endl;
		return;
	}
	Cell * temp = AllCells.find(make_pair(x, y))->second;
	vector<Cell *> result = temp->findFreeCell();
	for (unsigned int i = 0; i < result.size(); ++i) {
		cout << "(" << result[i]->x() << "," << result[i]->y() << ") ";
	}
	cout << endl;
}

void Game::initRooms(int x, int y, map< pair<int, int>, Cell *> &room) {
	if (theDisplay->display_at(x, y) == '.') {
		map< pair<int, int>, Cell *>::iterator it = room.find(make_pair(x,y));
		if ( it == room.end() ) {
			Cell * newcell = new Cell(x, y, '.', true);
			room[make_pair(x,y)] = newcell;
			AllCells[make_pair(x, y)] = newcell;
			// Up
			initRooms(x-1, y, room);
			// Down
			initRooms(x+1, y, room);
			// Left
			initRooms(x, y-1, room);
			// Right
			initRooms(x, y+1, room);
		}
	}
}

void Game::initPassage(int x, int y, map< pair<int, int>, Cell *> &room) {
	char c = theDisplay->display_at(x, y);
	if ( c == '+' || c == '#') {
		map< pair<int, int>, Cell *>::iterator it = room.find(make_pair(x, y));
		if (it == room.end()) {
			Cell * newcell = new Cell(x, y, c, false);
			newcell->addDisplay(theDisplay);
			room[make_pair(x, y)] = newcell;
			AllCells[make_pair(x, y)] = newcell;
			// Up
			initPassage(x - 1, y, room);
			// Down
			initPassage(x + 1, y, room);
			// Left
			initPassage(x, y - 1, room);
			// Right
			initPassage(x, y + 1, room);
		}
	}
}

void Game::initNeighbours() {
	for (map <pair<int, int>, Cell *>::iterator it = AllCells.begin(); it != AllCells.end(); ++it) {
		int x = it->first.first;
		int y = it->first.second;
		if (AllCells.find(make_pair(x - 1, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y - 1)]);
		}
		if (AllCells.find(make_pair(x - 1, y)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y)]);
		}
		if (AllCells.find(make_pair(x - 1, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x - 1, y + 1)]);
		}
		if (AllCells.find(make_pair(x, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x, y - 1)]);
		}
		if (AllCells.find(make_pair(x, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x, y + 1)]);
		}
		if (AllCells.find(make_pair(x + 1, y - 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y - 1)]);
		}
		if (AllCells.find(make_pair(x  + 1, y)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y)]);
		}
		if (AllCells.find(make_pair(x + 1, y + 1)) != AllCells.end()) {
			it->second->addNeighbour(AllCells[make_pair(x + 1, y + 1)]);
		}
	}
}

void Game::play() {
	string s;
	cout << "Hi welcome to UNDERTALE (jks this just means I haven't finished CC3K)" << endl;
	cout << "What would you like to play today?" << endl;
	cout << "k) knight" << endl;
	cout << "s) samurai" << endl;
	cout << "w) wizard" << endl;
	cin >> s;
	spawn_map(s);
	print();
	// Command Interperter:
	while (1) {
		cin >> s;
		if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
			if (move(s)) {
				if (getHP() <= 0) {
					cout << "You died. The chambers of CC3K has bested you. You are empty with determination." << endl;
					break;
				}
				print();
				//tick();
			}
		}
		else if (s == "u") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				cout << "Tried to use in direction " << s << endl;
				// USE + DIRECTION COMMAND NOT CODED YET
			}
			else {
				cout << "Invalid Command" << endl;
			}
		}
		else if (s == "a") {
			cin >> s;
			if (s == "nw" || s == "no" || s == "ne" || s == "we" || s == "ea" || s == "sw" || s == "so" || s == "se") {
				if (command_a(s)) {
					if (getHP() <= 0) {
						cout << "You died. The chambers of CC3K has bested you. You are empty with determination." << endl;
						break;
					}
					print();
					//tick();
				}
			}
			else {
				cout << "Invalid Command" << endl;
			}
		}
		else if (s == "q") {
			cout << "You have quit the game." << endl;
			break;
		}
		else {
			cout << "Invalid Command" << endl;
		}
	}
	cout << "The Game has ended" << endl;
}

bool Game::move(string direction) {
	if (player->move(direction)) {
		turns++;
		player->tick();
		for (unsigned int i = 0; i < Monster_List.size(); ++i) {
			Monster_List.at(i)->tick();
		}
		return true;
	}
	return false;
}
//	 will call Player->Move and will call Tick()

bool Game::command_a(string dir) {
	Cell * location = player->getLocation();
	std::vector<Cell *> neighbour = location->getNeighbours();
	int index = 246;
	int x = location->x();
	int y = location->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}
	if (index == 246) {
		std::cout << "Nothing to Attack" << std::endl;
		return false;
	}
	else if (neighbour.at(index)->get_contents_name() == "nothing") {
		std::cout << "Nothing to Attack" << std::endl;
		return false;
	}
	player->attack(neighbour.at(index)->getContents());
	player->tick();
	turns++;
	for (unsigned int i = 0; i < Monster_List.size(); ++i) {
		Monster_List.at(i)->tick();
		if (Monster_List.at(i)->getStatus() == "dead") {
			delete Monster_List.at(i);
			Monster_List.erase(Monster_List.begin() + i);
		}
	}
	return true;
}

int Game::getTurns() {
	return turns;
}
int Game::getHP() {
	return player->getHealth();
}

void Game::spawn_map(string s) {
	int player_chamber = prng(4);
	spawnplayer(s,player_chamber);
	spawnmonster(20);
	//spawnstairs()

}

void Game::spawnplayer(string s, int chamber) {
	while (true) {
		if (s == "k" || s == "s" || s == "w" ) {
			int cell;
			Cell * spawn_location = Room0.begin()->second;
			string job;
			if (s == "k") { job = "Knight"; }
			if (s == "s") { job = "Samurai"; }
			if (s == "w") { job = "Wizard"; }
			if (chamber == 0) { 
				cell = prng(Room0.size()-1);
				// TAKE THIS OUT.
				cout << "Spawn Location should be Room: " << chamber << " Cell: " << cell << endl;
				for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 1) { 
				cell = prng(Room1.size()-1);
				// TAKE THIS OUT
				cout << "Spawn Location should be Room: " << chamber << " Cell: " << cell << endl;
				for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 2) { 
				cell = prng(Room2.size()-1); 
				// TAKE THIS OUT
				cout << "Spawn Location should be Room: " << chamber << " Cell: " << cell << endl;
				for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 3) {
				cell = prng(Room3.size()-1);
				// TAKE THIS OUT
				cout << "Spawn Location should be Room: " << chamber << " Cell: " << cell << endl;
				for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 4) { 
				cell = prng(Room4.size()-1);
				// TAKE THIS OUT
				cout << "Spawn Location should be Room: " << chamber << " Cell: " << cell << endl;
				for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			player = new Character(job, spawn_location);
			cout << "You have chosen to play as a " << job <<". Good luck." << endl;
			cout << "You are filled with DETERMINATION!" << endl;
			break;
		}
		cout << "Invalid Choice. Try Again" << endl;
		cin >> s;
	}
}

void Game::spawnmonster(int NUM_MONSTERS) {
	for (int i = 0; i < NUM_MONSTERS; ++i) {
		int monster_type = prng(5); // Not used but its here :) since I only have Orcs so far.
		Cell * spawn_location = Room0.begin()->second;
		while (true) {
			int chamber = prng(4);
			int cell;
			if (chamber == 0) {
				cell = prng(Room0.size() - 1);
				for (map<pair<int, int>, Cell*>::iterator it = Room0.begin(); it != Room0.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 1) {
				cell = prng(Room1.size() - 1);
				for (map<pair<int, int>, Cell*>::iterator it = Room1.begin(); it != Room1.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 2) {
				cell = prng(Room2.size() - 1);
				for (map<pair<int, int>, Cell*>::iterator it = Room2.begin(); it != Room2.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 3) {
				cell = prng(Room3.size() - 1);
				for (map<pair<int, int>, Cell*>::iterator it = Room3.begin(); it != Room3.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			if (chamber == 4) {
				cell = prng(Room4.size() - 1);
				for (map<pair<int, int>, Cell*>::iterator it = Room4.begin(); it != Room4.end(); ++it) {
					if (cell == 0) {
						spawn_location = it->second;
						break;
					}
					cell--;
				}
			}
			// Checking if the cell is valid
			if (spawn_location->isEmpty()) { break; }
		}
		GameObject * monster = new Orc(spawn_location);
		Monster_List.push_back(monster);
	}
}

void Game::spawnstairs() {

}
void Game::spawnpotion(int NUM_POTIONS) {

}
void Game::spawngold(int NUM_GOLD) {

}










