#include "cell.h"

Display * Cell::board = 0;

void Cell::addDisplay(Display * board) {
	Cell::board = board;
}

Cell::Cell(int x, int y, char display, bool travel) {
	_x = x;
	_y = y;
	_display = display;
	_contents = 0;
	monster_travelable = travel;
	_defaultdisplay = display;
}

int Cell::x() {
	return _x;
}

int Cell::y() {
	return _y;
}

char Cell::display() {
	return _display;
}

bool Cell::isEmpty() {
	if (_contents == 0) {
		return true;
	}
	return false;
}

void Cell::setDisplay(char c) {
	_display = c;
	board->notify(_x, _y, c);
}
void Cell::setContent(GameObject * object) {
	_contents = object;
}
void Cell::removeOjbect() {
	_contents = 0;
	_display = _defaultdisplay;
	board->notify(_x, _y, _display);
}
void Cell::addNeighbour(Cell * neighbour) {
	neighbours.push_back(neighbour);
}

std::vector<Cell *> Cell::getNeighbours() {
	return neighbours;
}

std::vector<Cell *> Cell::findFreeCell() {
	std::vector<Cell *> freeCells;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->isEmpty() && neighbours.at(i)->getMonTravel()) {
				freeCells.push_back(neighbours.at(i));
		}
	}
	return freeCells;
}

bool Cell::getMonTravel() {
	return monster_travelable;
}

std::string Cell::get_contents_name() {
	if (_contents == 0) { return "nothing"; }
	else {
		return _contents->getName();
	}
}

GameObject * Cell::getContents() {
	return _contents;
}
