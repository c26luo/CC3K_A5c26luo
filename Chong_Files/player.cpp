#include "player.h"

Character::Character(std::string c, Cell * cell) {
	if (c == "Knight") {
		_class = c;
		maxhp = 100;
		_hp = 100;
		_att = 50;
		_def = 50;
	}
	else if (c == "Samurai") {
		_class = c;
		maxhp = 80;
		_hp = 80;
		_att = 50;
		_def = 15;
	}
	else {
		_class = c;
		maxhp = 60;
		_hp = 60;
		_att = 25;
		_def = 0;
	}
	location = cell;
	location->setContent(this);
	location->setDisplay('@');
}

// Get
int Character::getHealth() {
	return _hp;
}

int Character::getMaxHP() {
	return maxhp;
}

int Character::getAttack() {
	return _att;
}

std::string Character::getStatus() {
	return status;
}

int Character::getDefence() {
	return _def;
}
std::string Character::getName() {
	return name;
}
Cell * Character::getLocation() {
	return location;
}

//Set
void Character::setHealth(int n){
	_hp = n;
}
void Character::setAttack(int n){
	_att = n;
}
void Character::setDefence(int n){
	_def = n;
}

int Character::damage(int attker, int defender) {
	int formula = ((attker * (100 - defender)) / 100);
	return ceil(formula);
}

bool Character::move(std::string dir) {
	std::vector<Cell *> neighbour = location->getNeighbours();
	int index = 246;
	int x = location->x();
	int y = location->y();
	for (unsigned int i = 0; i < neighbour.size(); ++i) {
		int vx = neighbour[i]->x();
		int vy = neighbour[i]->y();
		if (vx == x - 1 && vy == y - 1 && dir == "nw") { index = i; }
		else if (vx == x - 1 && vy == y && dir == "no") { index = i; }
		else if (vx == x - 1 && vy == y + 1 && dir == "ne") { index = i; }
		else if (vx == x && vy == y - 1 && dir == "we") { index = i; }
		else if (vx == x && vy == y + 1 && dir == "ea") { index = i; }
		else if (vx == x + 1 && vy == y - 1 && dir == "sw") { index = i; }
		else if (vx == x + 1 && vy == y && dir == "so") { index = i; }
		else if (vx == x + 1 && vy == y + 1 && dir == "se") { index = i; }
	}

	if (index == 246) {
		std::cout << "Invalid Move" << std::endl;
		return false;
	}

	Cell * newlocation = neighbour.at(index);
	if (newlocation->isEmpty()) {
		newlocation->setContent(this);
		newlocation->setDisplay('@');
		location->removeOjbect();
		location = newlocation;
	}
	else {
		std::cout << "Invalid Move" << std::endl;
		return false;
	}
	return true;
}

void Character::attack(GameObject * target) {
	int dmg = damage(_att, target->getDefence());
	std::cout << "You dealt " << dmg << " to the Orc!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Character::tick() {
	if (_hp == maxhp) {
		return;
	}
	else if (_hp + 5 >= maxhp) {
		_hp = maxhp;
	}
	else {
		_hp += 5;
	}
}
