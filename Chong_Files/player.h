#ifndef PLAYER_H
#define PLAYER_H
#include "game_object.h"

class Character : public GameObject {
	std::string name = "player";
	std::string _class;
	std::string status = "alive";
	int maxhp;
	int _att;
	int _hp;
	int _def;
	Cell * location;

public:
	Character(std::string c, Cell *);

	// Get
	int getHealth();
	int getMaxHP();
	int getAttack();
	int getDefence();
	std::string getStatus();
	std::string getName();
	Cell* getLocation();

	//Set
	void setHealth(int);
	void setAttack(int);
	void setDefence(int);

	int damage(int attacker, int defender);
	void attack(GameObject * target);
	bool move(std::string dir);
	void tick();

};

#endif


