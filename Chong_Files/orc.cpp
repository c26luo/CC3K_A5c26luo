#include "orc.h"

Orc::Orc(Cell * spawn_location) {
	_display = 'O';
	setHealth(120);
	setAttack(30);
	setDefence(30);
	setName("Orc");
	setStatus("alive");
	location = spawn_location;
	location->setContent(this);
	location->setDisplay(_display);
}

int Orc::damage(int attker, int defender) {
	int formula = ((attker * (100 - defender)) / 100);
	return ceil(formula);
}
void Orc::attack(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The Orc dealt " << dmg << " damage to you!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Orc::tick() {
	if (getHealth() <= 0) {
		location->removeOjbect();
		setStatus("dead");
		std::cout << "You killed the Orc!" << std::endl;
		return;
	}
	std::vector<Cell*> neighbours = location->getNeighbours();
	bool is_player_nearby = false;
	Cell * player_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			player_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby) {
		attack(player_location->getContents());
	}
	else {
		std::vector<Cell *> moveable_spots = location->findFreeCell();
		int direction = prng(moveable_spots.size() - 1);
		Cell * newlocation = moveable_spots.at(direction);
		newlocation->setContent(this);
		newlocation->setDisplay(_display);
		location->removeOjbect();
		location = newlocation;
	}
}
