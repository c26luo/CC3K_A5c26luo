#ifndef GAME_H
#define GAME_H
#include "prng.h"
#include "display.h"
#include "cell.h"
#include "game_object.h"
#include "player.h"
#include "monster.h"
#include "orc.h"
#include <map>
#include <fstream>
using namespace std;

extern PRNG prng;

class Game {
	// Object Maps
	map< pair<int, int>, Cell *> Room0;
	map< pair<int, int>, Cell *> Room1;
	map< pair<int, int>, Cell *> Room2;
	map< pair<int, int>, Cell *> Room3;
	map< pair<int, int>, Cell *> Room4;
	map< pair<int, int>, Cell *> Passage;
	map< pair<int, int>, Cell *> AllCells;
	vector<GameObject *> Monster_List;

	//Data 
	int width, height;
	int turns;
	Display * theDisplay;
	Character * player;

// Private Functions:
	void initRooms(int, int, map< pair<int, int>, Cell *> &);
	void initPassage(int, int, map< pair<int, int>, Cell *> &);
	void initNeighbours();
	bool command_a(string dir);
	bool move(string direction);
	int getTurns();
	int getHP();

	void spawn_map(string s);
	void spawnplayer(string s, int chamber);
	void spawnstairs();
	void spawnpotion(int);
	void spawngold(int);
	void spawnmonster(int);

public:
	Game(ifstream& infile);
	~Game();
	void print();
	void play();

	//void resetFloor();
	// The following Methods needs GameObject Class and its Subclass to implement:
	//void command_u(string direction);
		//	 will call Player->Action and will call Tick()
		//	 will call Player->Attack and will call Tick()
	// int getStats();
	// bool isDead();

	//Testing purposes only
	void findFreeCells(int x, int y);
	Display * getDisplay();
};

#endif
