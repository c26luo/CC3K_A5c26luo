#include "monster.h"

class Orc : public Monster {
	char _display;
	Cell * location;
public:
	Orc(Cell * spawn_location);
	int damage(int attker, int defender);
	void attack(GameObject * target);
	void tick();
};