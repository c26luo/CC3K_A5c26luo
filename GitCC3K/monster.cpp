#include "monster.h"

// Get
int Monster::getHealth() {
	return _hp;
}
int Monster::getAttack() {
	return _att;
}
int Monster::getDefence() {
	return _def;
}
std::string Monster::getName() {
	return name;
}
std::string Monster::getStatus() {
	return status;
}

//Set
void Monster::setHealth(int hp) {
	_hp = hp;
}
void Monster::setAttack(int att) {
	_att = att;
}
void Monster::setDefence(int def) {
	_def = def;
}
void Monster::setName(std::string s) {
	name = s;
}
void Monster::setStatus(std::string s) {
	status = s;
}