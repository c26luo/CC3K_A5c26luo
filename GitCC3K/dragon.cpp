#include "dragon.h"

Dragon::Dragon(Cell * spawn_location) {
	_display = 'D';
	setHealth(150);
	setAttack(50);
	setDefence(10);
	setName("Dragon");
	setStatus("alive");
	location = spawn_location;
	location->setContent(this);
	location->setDisplay(_display);

}
int Dragon::damage(int attker, int defender) {
	int formula = ((attker * (100 - defender)) / 100);
	return ceil(formula);
}
void Dragon::attack(GameObject * target) {
	int dmg = damage(getAttack(), target->getDefence());
	std::cout << "The Dragon dealt " << dmg << " damage to you!" << std::endl;
	target->setHealth(target->getHealth() - dmg);
}

void Dragon::tick() {
	if (getHealth() <= 0) {
		location->removeOjbect();
		setStatus("dead");
		std::cout << "You killed the Dragon!" << std::endl;
		return;
	}
	std::vector<Cell*> neighbours = location->getNeighbours();
	bool is_player_nearby = false;
	Cell * player_location = 0;
	for (unsigned int i = 0; i < neighbours.size(); ++i) {
		if (neighbours.at(i)->get_contents_name() == "player") {
			is_player_nearby = true;
			player_location = neighbours.at(i);
			break;
		}
	}
	if (is_player_nearby) {
		attack(player_location->getContents());
	}
	/*
	else {
		std::vector<Cell *> moveable_spots = location->findFreeCell();
		int direction = prng(moveable_spots.size() - 1);
		Cell * newlocation = moveable_spots.at(direction);
		newlocation->setContent(this);
		newlocation->setDisplay(_display);
		location->removeOjbect();
		location = newlocation;
	}
	*/
}
