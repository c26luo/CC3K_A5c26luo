#ifndef TREASURE_H
#define TREASURE_H

#include "game_object.h"

class Treasure : public GameObject{
	int amount;
	char _display;
	std::string name;
	Cell *location;
public:
	//getter setters 
	void setName(std::string);
	void setAmount(int);
	void setDisplay(char);
	void setLocation(Cell *);
	std::string getName();

	// USELESS SHIT ONLY HERE TO OVERRIDE GAMEOBJECT VIRTUALS.
	int getAttack() { return 0; }
	int getDefence() { return 0; }
	int getHealth() { return 0; }
	void setHealth(int) {}
	void setAttack(int) {}
	void setDefence(int) {}
	void tick() {}
	void attack(GameObject *) {}
	std::string getStatus() { return ""; }
};

#endif