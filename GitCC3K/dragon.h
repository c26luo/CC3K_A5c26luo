#ifndef DRAGON_H
#define DRAGON_H
#include "monster.h"

class Dragon : public Monster {
	char _display;
	Cell * location;

public:
	Dragon(Cell * spawn_location);
	int damage(int attker, int defender);
	void attack(GameObject * target);
	void tick();
};

#endif