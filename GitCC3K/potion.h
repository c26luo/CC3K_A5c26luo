#ifndef POTION_H
#define POTION_H
#include "game_object.h"

class Potion : public GameObject {
	std::string name;
	std::string potion_type;
	int stat_value;
	char _display; 
public:
	Potion(int type, Cell * spawn_location);
	void attack(GameObject *);
	std::string getName();
	std::string getStatus();

	// USELESS SHIT ONLY HERE TO OVERRIDE GAMEOBJECT VIRTUALS.
	int getAttack() { return 0; }
	int getDefence() { return 0; }
	int getHealth() { return 0; }
	void setHealth(int) {}
	void setAttack(int) {}
	void setDefence(int) {}
	void tick() {}
};

#endif