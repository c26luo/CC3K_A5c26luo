#include "potion.h"

Potion::Potion(int type, Cell * spawn_location) {
	_display = '!';
	name = "potion";
	if (type == 0) {
		// Restore Health RH increase by 30
		stat_value = 30;
		potion_type = "RH";
	}
	if (type == 1) {
		// Boost Atk BA increase ATK by 10
		stat_value = 10;
		potion_type = "BA";
	}
	if (type == 2) {
		// Boost Def BD increase Def by 10
		stat_value = 10;
		potion_type = "BD";
	}
	if (type == 3) {
		//Poison health PH decrease HP by 15
		stat_value = -15;
		potion_type = "PH";
	}
	if (type == 4) {
		//Wound Atk WA decrease Atk by 5
		stat_value = -5;
		potion_type = "WA";
	}
	if (type == 5) {
		//Wound Def WD decrease Def by 5
		stat_value = -5;
		potion_type = "WD";
	}
	spawn_location->setContent(this);
	spawn_location->setDisplay(_display);
}

void Potion::attack(GameObject * thing) {
	if (potion_type == "RH") {
		std::cout << "You drank the Restore Health Potion and restored 30 HP." << std::endl;
		thing->setHealth(thing->getHealth() + stat_value);
	}
	if (potion_type == "BA") {
		std::cout << "You drank the Boost ATK Potion! You are filled with DETERMINATION." << std::endl;
		std::cout << "ATK has been increased by 10." << std::endl;
		thing->setAttack(thing->getAttack() + stat_value);
	}
	if (potion_type == "BD") {
		std::cout << "You drank the Boost Def Potion! You are filled with DETERMINATION." << std::endl; 
		std::cout << "Def has been increased by 10." << std::endl;
		thing->setDefence(thing->getDefence() + stat_value);
	}
	if (potion_type == "PH") {
		std::cout << "You drank the Poison Health Potion! OH NO YOU ARE POISONED AND LOST 15 HP!" << std::endl;
		std::cout << "Well this sucks! Whatever, you are filled with DETERMINATION." << std::endl;
		thing->setHealth(thing->getHealth() + stat_value);
	}
	if (potion_type == "WA") {
		std::cout << "You drank the Wound ATK Potion. You lost 5 ATK." << std::endl;
		std::cout << "Your attacks are as weak as shit!" << std::endl;
		thing->setAttack(thing->getAttack() + stat_value);
	}
	if (potion_type == "WD") {
		std::cout << "You drank the Wound Def Potion. You lost 5 Def." << std::endl;
		std::cout << "You stripped off your armor because it was too heavy and you forgot deodorant." << std::endl;
		thing->setDefence(thing->getDefence() + stat_value);
	}
}
std::string Potion::getName() {
	return name;
}
std::string Potion::getStatus() {
	return potion_type;
}