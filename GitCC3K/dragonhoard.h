#ifndef DRAGONHOARD_H
#define DRAGONHOARD_H
#include "treasure.h"
#include "prng.h"
#include "dragon.h"
#include <vector>

class DragonHoard : public Treasure {
	char _display;
	Cell *location;
	bool guarded;
	Dragon * dragon;
	public:
		DragonHoard(Cell *spawn_location);
		void setGuard();
		bool notGuarded();
};

#endif