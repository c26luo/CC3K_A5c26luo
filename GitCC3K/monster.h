#ifndef MONSTER_H
#define MONSTER_H
#include "game_object.h"
#include "prng.h"

extern PRNG prng;

class Monster : public GameObject {
	std::string name;
	std::string status;
	int maxhp;
	int _att;
	int _hp;
	int _def;
public:
	// Get
	int getHealth();
	int getAttack();
	int getDefence();
	std::string getName();
	std::string getStatus();
	//Set
	void setHealth(int);
	void setAttack(int);
	void setDefence(int);
	void setName(std::string);
	void setStatus(std::string);

	virtual int damage(int attacker, int defender) = 0;
	virtual void attack(GameObject * target) = 0;
	virtual void tick() = 0;
};

#endif