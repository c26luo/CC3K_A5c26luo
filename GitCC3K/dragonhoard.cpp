#include "dragonhoard.h"

DragonHoard::DragonHoard(Cell *spawn_location) {
	_display = '$';
	setName("dragon hoard");
	setAmount(50);
	location = spawn_location;
	location->setContent(this);
	location->setDisplay(_display);

	std::vector <Cell *> dragon_locations = spawn_location->findFreeCell();
	Cell *dragon_spawn_location = dragon_locations.at(prng( dragon_locations.size() - 1));
	Dragon * dragon = new Dragon(dragon_spawn_location);
	guarded = true;
}

void DragonHoard::setGuard() {
	if (dragon->getStatus() == "dead") guarded = false;
}

bool DragonHoard::notGuarded() {
	return guarded;
}